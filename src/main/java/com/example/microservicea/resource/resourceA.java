package com.example.microservicea.resource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class resourceA {

    @Value("${server.port}")
    private String port;

    @RequestMapping(method = RequestMethod.GET, value = "/a")
    public String resourceA() {
        return String.format("You hit microservice A on port %s", port);
    }

}
